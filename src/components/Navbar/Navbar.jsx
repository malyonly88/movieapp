import "../../assets/style/navbar.scss";
import { NavLink } from "react-router-dom";
import { useState } from "react";
import logo from "../../assets/images/logo.png";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUserCircle, faSearch } from "@fortawesome/free-solid-svg-icons";




export default function Navbar() {
  const [menus] = useState([
    {
      path: '/',
      name: 'Movie'
    },
    {
      path: 'cinemas',
      name: 'Cinemas'
    },
    {
      path: 'contact',
      name: 'Contact'
    },
  ])
  return (

    <div>
      <nav class="bg-gradient-to-r from-black to-[#d10b57] border-gray-200 px-2 sm:px-4 py-2.5 dark:bg-gray-900">
        <div class="container flex flex-wrap items-center justify-between mx-auto">
          <a href="https://flowbite.com/" class="flex items-center">
            <img src={logo} alt="logo" class="h-6 mr-3 sm:h-9" />
          </a>
          <div class="flex md:order-2 text-white">
            <div><FontAwesomeIcon icon={faSearch} /></div>
            <div className="pl-5"><FontAwesomeIcon icon={faUserCircle} /></div>
          </div>
          <div class="items-center justify-between hidden w-full md:flex md:w-auto md:order-1" id="navbar-cta">
            <ul class="flex flex-col p-4 mt-4 border border-gray-100 rounded-lg  md:flex-row md:space-x-8 md:mt-0 md:text-sm md:font-medium md:border-0  dark:bg-gray-800 md:dark:bg-gray-900 dark:border-gray-700">
              {
                menus.map((menu, index) => {
                  return (
                    <li key={index}>
                      <a href="#" class="block py-2 pl-3 pr-4 text-white rounded hover:bg-gray-100 md:hover:bg-transparent md:hover:text-blue-700 md:p-0 md:dark:hover:text-white dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent dark:border-gray-700"><NavLink to={menu.path}>{menu.name}</NavLink></a>
                    </li>
                  );
                })
              }
            </ul>
          </div>
        </div>
      </nav>
    </div>

    // <div>
    //   <nav className="navbar">
    //     <div className="navbar-container container">
    //       <div className="logo">
    //         <img src={logo} alt="logo" />
    //       </div>
    //       <ul>
    //         {
    //           menus.map((menu, index) => {
    //             return (
    //               <li key={index}>
    //                 <NavLink to={menu.path}>{menu.name}</NavLink>
    //               </li>
    //             );
    //           })
    //         }
    //       </ul>
    //       <div className="navbar-action">
    //         <button>
    //           <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 24 24"><path fill="currentColor" d="m19.6 21l-6.3-6.3q-.75.6-1.725.95Q10.6 16 9.5 16q-2.725 0-4.612-1.887Q3 12.225 3 9.5q0-2.725 1.888-4.613Q6.775 3 9.5 3t4.613 1.887Q16 6.775 16 9.5q0 1.1-.35 2.075q-.35.975-.95 1.725l6.3 6.3ZM9.5 14q1.875 0 3.188-1.312Q14 11.375 14 9.5q0-1.875-1.312-3.188Q11.375 5 9.5 5Q7.625 5 6.312 6.312Q5 7.625 5 9.5q0 1.875 1.312 3.188Q7.625 14 9.5 14Z" /></svg>
    //         </button>
    //         <button>
    //           <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 24 24"><path fill="currentColor" d="M5.85 17.1q1.275-.975 2.85-1.538Q10.275 15 12 15q1.725 0 3.3.562q1.575.563 2.85 1.538q.875-1.025 1.363-2.325Q20 13.475 20 12q0-3.325-2.337-5.663Q15.325 4 12 4T6.338 6.337Q4 8.675 4 12q0 1.475.488 2.775q.487 1.3 1.362 2.325ZM12 13q-1.475 0-2.488-1.012Q8.5 10.975 8.5 9.5t1.012-2.488Q10.525 6 12 6t2.488 1.012Q15.5 8.025 15.5 9.5t-1.012 2.488Q13.475 13 12 13Zm0 9q-2.075 0-3.9-.788q-1.825-.787-3.175-2.137q-1.35-1.35-2.137-3.175Q2 14.075 2 12t.788-3.9q.787-1.825 2.137-3.175q1.35-1.35 3.175-2.138Q9.925 2 12 2t3.9.787q1.825.788 3.175 2.138q1.35 1.35 2.137 3.175Q22 9.925 22 12t-.788 3.9q-.787 1.825-2.137 3.175q-1.35 1.35-3.175 2.137Q14.075 22 12 22Z" /></svg>
    //         </button>
    //       </div>
    //     </div>
    //   </nav>
    // </div>
  );
}
