import React from "react";
import logo from "../../assets/images/logo.png";
import "../../assets/style/cinemas.css";
import "../../assets/style/footer.css"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLocationDot, faPhoneVolume, faEnvelope } from "@fortawesome/free-solid-svg-icons";
import { faFacebook, faInstagram, faYoutube, faLinkedin, faTwitter } from "@fortawesome/free-brands-svg-icons";

export default function Movie() {
    const year = new Date().getFullYear();

    return (
        <>
            <div className="">
                
                <footer class="p-20 bg-gradient-to-r from-black to-pink-900 sm:p-6 dark:bg-gray-900">
                    <div class="pt-20 md:flex md:justify-around">
                        <div class="mb-6 md:mb-0">
                            <a href="https://flowbite.com/" class="flex items-center">
                                <img src={logo} alt="" className="w-[180px] mb-[30px] " />
                            </a>
                            <div class="flex pt-10 space-x-6 sm:justify-center sm:mt-0">
                                <a href="#" class="text-white hover:text-gray-900 dark:hover:text-white">
                                    <FontAwesomeIcon icon={faFacebook} className=" hover:text-sky-700 text-[30px]" fill="currentColor" aria-hidden="true" />
                                    <span class="sr-only">Facebook page</span>
                                </a>
                                <a href="#" class="text-white hover:text-gray-900 dark:hover:text-white">
                                    <FontAwesomeIcon icon={faInstagram} className="text-white hover:text-pink-600 text-[30px]" />
                                    <span class="sr-only">Twitter page</span>
                                </a>
                                <a href="#" class="text-white hover:text-gray-900 dark:hover:text-white">
                                    <FontAwesomeIcon icon={faLinkedin} className=" hover:text-blue-700 text-[30px]" />
                                    <span class="sr-only">GitHub account</span>
                                </a>
                                <a href="#" class="text-white hover:text-gray-900 dark:hover:text-white">
                                    <FontAwesomeIcon icon={faTwitter} className=" hover:text-sky-500 text-[30px]" />
                                    <span class="sr-only">Dribbbel account</span>
                                </a>
                                <a href="#" class="text-white hover:text-gray-900 dark:hover:text-white">
                                    <FontAwesomeIcon icon={faYoutube} className=" hover:text-red-700 text-[30px]" />
                                    <span class="sr-only">Instagram page</span>
                                </a>
                            </div>
                        </div>
                        <div class="grid grid-cols-4 gap-8 sm:gap-7 sm:grid-cols-4">
                            <div className="ml-10">
                                <h2 class="mb-6 text-[20px] font-semibold text-white uppercase dark:text-white">Company</h2>
                                <ul class="text-white dark:text-gray-400 text-[14px]">
                                    <li class="mb-4">
                                        <a href="https://flowbite.com/" class="hover:text-gray-500">Movie</a>
                                    </li>
                                    <li class="mb-4">
                                        <a href="https://tailwindcss.com/" class="hover:text-gray-500">Cinemas</a>
                                    </li>
                                    <li class="mb-4">
                                        <a href="https://tailwindcss.com/" class="hover:text-gray-500">About</a>
                                    </li>
                                    <li>
                                        <a href="https://tailwindcss.com/" class="hover:text-gray-500">Contact</a>
                                    </li>
                                </ul>
                            </div>
                            <div className="ml-10">
                                <h2 class="mb-6 text-[20px] font-semibold text-white uppercase dark:text-white">Download</h2>
                                <ul class="text-white dark:text-gray-400 text-[14px]">
                                    <li class="mb-4">
                                        <a href="https://flowbite.com/" class="hover:text-gray-500">App</a>
                                    </li>
                                    <li>
                                        <a href="https://tailwindcss.com/" class="hover:text-gray-500">Devices</a>
                                    </li>
                                </ul>
                            </div>
                            <div className="ml-10">
                                <h2 class="mb-6 text-[20px] font-semibold text-white uppercase dark:text-white">Support</h2>
                                <ul class="text-white dark:text-gray-400 text-[14px]">
                                    <li class="mb-4">
                                        <a href="https://flowbite.com/" class="hover:text-gray-500">Help Center</a>
                                    </li>
                                    <li>
                                        <a href="https://tailwindcss.com/" class="hover:text-gray-500">Term Of US</a>
                                    </li>
                                </ul>
                            </div>
                            <div className="ml-10">
                                <h2 class="mb-6 text-[20px] font-semibold text-white uppercase dark:text-white">Legal</h2>
                                <ul class="text-white dark:text-gray-400 text-[14px]">
                                    <li class="mb-4">
                                        <a href="#" class="hover:text-gray-500">Privacy Policy</a>
                                    </li>
                                    <li>
                                        <a href="#" class="hover:text-gray-500">Terms &amp; Conditions</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <hr class="my-6 border-gray-200 sm:mx-auto dark:border-gray-700 lg:my-8" />
                    <div class="sm:flex sm:items-center sm:justify-center text-center">
                        <span class="text-sm text-gray-500 sm:text-center dark:text-gray-400">© 2023 <a href="#" class="hover:underline">TheTrailer™</a>. All Rights Reserved.
                        </span>
                    </div>
                </footer>

            </div>


        </>
    );
}
