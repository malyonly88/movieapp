import React from "react";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from "react-responsive-carousel";
import "../../assets/style/slider.css";
// import { ReactVideo } from "reactjs-media";
import vdo1 from "../../assets/images/vdo1.mp4";
import vdo2 from "../../assets/images/vdo2.mp4";
import vdo3 from "../../assets/images/vdo3.mp4";


export default function Slider() {
  return (
    <div className="p-0 bg-gradient-to-r from-black to-pink-900">
      <Carousel infiniteLoop autoPlay>
        <div className="vdo">
          <video
            src={vdo1}
            autoPlay
            loop
            muted
          />
        </div>
        <div className="img">
          <video
            src={vdo2}
            autoPlay
            loop
            muted
          />
        </div>
        <div className="img">
          <video
            src={vdo3}
            autoPlay
            loop
            muted
          />
        </div>
      </Carousel>
    </div>
  );
}