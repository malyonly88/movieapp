import React, { useState } from "react";
import { useEffect } from "react";
import axios from "axios";
import Slider from "./Slider";
import "../../assets/style/cinemas.css";
import Footer from "../Footer/Footer";


export default function Movie() {
  const [page, setPage] = React.useState(1);
  const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {
    setPage(value);
  };

  const [movies, setMovies] = useState([]);
  useEffect(() => {
    axios
      .get(
        "https://api.themoviedb.org/3/movie/popular?api_key=92791d8d2dbc781d5cba4cbf531dd04b&page=1",
        {
          params: {
            api_key: process.env.REACT_APP_API_KEY,
            page: 1,
          },
        }
      )
      .then((data) => setMovies(data.data.results));
  }, []);
  // const [ data ] = useFetch ('https://api.themoviedb.org/3/movie/popular?api_key=92791d8d2dbc781d5cba4cbf531dd04b&page=1')
  return (
    <>
      <div>
        <Slider />
      </div>
      <div className="p-20 pl-20 pt-5 bg text-center bg-gradient-to-r from-black to-pink-900">
        {movies.map((item, index) => {
          return (
            <div
              key={index}
              className="row inline-grid grid-rows-none gap-4 p-1"
            >
              <div className="column">
                <div className="card mt-0 mb-0">
                  <div className="btn">
                    <img
                      src={
                        process.env.REACT_APP_FILE_URL + item.poster_path
                      }
                      alt=""
                    />
                    <div className="socail-link p-2">
                      <p className="text-left pt-1 pl-1 text-[12px] font-bold">
                        {item.title}
                      </p>
                      <h6 className="text-left pt-1 pl-1 text-[12px] font-bold">
                        <a href="" className="mr-3">
                          <i class="fa fa-eye text-yellow-400"></i>
                        </a>
                        {item.popularity}
                      </h6>
                      <h6 className="text-left pt-1 pl-1 text-[12px] font-bold">
                        <a href="" className="mr-3">
                          <i class="fa fa-calendar text-yellow-400"></i>
                        </a>
                        {item.release_date}
                      </h6>
                      <h6 className="text-left pt-1 pl-1 text-[12px] font-bold">
                        <a href="" className="mr-3">
                          <i class="fa fa-star text-yellow-400"></i>
                        </a>
                        {item.vote_average}
                      </h6>
                      <div class="grid grid-cols-3 gap-4  place-items-center mt-6">
                        <div><button class=" text-black font-bold hover:text-violet-600 active:text-violet-700 focus:outline-none">Info</button></div>
                        <div><button class=" text-black font-bold hover:text-yellow-400 active:text-yellow-700 focus:outline-none">Booking</button></div>
                      </div>
                    </div>
                  </div>
                  <br />
                </div>
              </div>
            </div>
          );
        })}
      </div>
      <div>
        <Footer/>
      </div>
    </>
  );
}
