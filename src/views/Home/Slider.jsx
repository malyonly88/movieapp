import React from "react";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from "react-responsive-carousel";
import "../../assets/style/slider.css";
import slide1 from "../../assets/images/slider-1.jpg";
import slide2 from "../../assets/images/slider-2.jpg";
import slide3 from "../../assets/images/slider-3.jpg";
import slide4 from "../../assets/images/slider-4.jpg";



export default function Slider() {
  return (
    <div className="p-0 bg-gradient-to-r from-black to-pink-900">
      <Carousel infiniteLoop autoPlay interval="6000" play={true}>
        <img src={slide1} alt="test" autoPlay/>
        <img src={slide2} alt="" autoPlay/>
        <img src={slide3} alt="" autoPlay/>
        <img src={slide4} alt="" autoPlay/>
        {/* <img src={slide1} alt="" /> */}
      </Carousel>
    </div>
  );
}
