// import React, { useState } from "react";
// import { useEffect } from "react";
// import axios from "axios";
// import swiperslider from "../../assets/style/swiperslider.css"



// export default function SwiperSlider() {
  
//   SwiperSlider('.swiper-container', {
//     pagination: '.swiper-pagination',
//     effect: 'coverflow',
//     grabCursor: true,
//     centeredSlides: true,
//     slidesPerView: 'auto',
//     coverflow: {
//         rotate: 20,
//         stretch: 0,
//         depth: 200,
//         modifier: 1,
//         slideShadows: true,
//     },
//     loop: true,
// });

//   const [page, setPage] = React.useState(1);
//   const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {
//     setPage(value);
//   };

//   const [movies, setMovies] = useState([]);
//   useEffect(() => {
//     axios
//       .get(
//         "https://api.themoviedb.org/3/movie/popular?api_key=92791d8d2dbc781d5cba4cbf531dd04b&page=1",
//         {
//           params: {
//             api_key: process.env.REACT_APP_API_KEY,
//             page: 1,
//           },
//         }
//       )
//       .then((data) => setMovies(data.data.results));
//   }, []);

//   return (
//     <>
//       <div className="swiper-container">
//         <div className="swiper-wrapper">
//             <div className="swiper-slide" ></div>
//             <div className="swiper-slide" ></div>
//         </div>
//         <div className="swiper-pagination"></div>
//     </div>
//     </>
//   );
// }