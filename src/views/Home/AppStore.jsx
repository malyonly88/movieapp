export default function AppStore() {
    return (
        <>
            <div className=" w-full justify-center text-center bg-gradient-to-r from-black to-pink-900 dark:bg-gray-900">
                <div className="p-10 grid grid-cols-2 gap-2 items-centershadow ">
                    <div className="flex justify-center">
                    <img className="object-cover w-[500px]" src="https://www.rappler.com/tachyon/2021/11/Bonifacio-High-Street-Cinemas.jpg" alt="" />
                    </div>
                    <div className="flex flex-col justify-center p-4 ">
                        <h5 className="mb-2 text-2xl font-bold tracking-tight text-white dark:text-white">VIP SITE</h5>
                        <p className="mb-3 font-normal text-white dark:text-gray-400">Booking right now and you can watch your favorite movie. <br /> Contact Us to get VIP SITE.</p>
                    </div>
                </div>
            </div>
        </>
    );
}