import React from "react";
import Slider from "./Slider";
import Footer from "../Footer/Footer";
import AppStore from "./AppStore";
// import SwiperSlider from "../Home/SwiperSlider";

import "../../assets/style/movie.css";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAndroid, faApple, faWindows } from "@fortawesome/free-brands-svg-icons";
import { faStar } from "@fortawesome/free-solid-svg-icons";



export default function Movie() {
  return (
    <>
      <div>
        <Slider />
      </div>


      <div class=" p-4 text-center bg-gray-800  shadow sm:p-4 dark:bg-gray-800 dark:border-gray-700">
        <p class="mb-5 text-base text-gray-500 sm:text-lg dark:text-gray-400">Watch anytime, anywhere with the The Trailer.</p>
        <div class="items-center justify-center space-y-4 sm:flex sm:space-y-0 sm:space-x-4">
          <div class=" sm:w-autofocus:ring-4 focus:outline-none focus:ring-gray-300 text-white inline-flex items-center justify-center px-4 py-2.5 dark:bg-gray-700 dark:hover:bg-gray-600 dark:focus:ring-gray-700">
            <FontAwesomeIcon icon={faApple} className="w-7 h-7" />
          </div><br />
          <div class="sm:w-autofocus:ring-4 focus:outline-none focus:ring-gray-300 text-white inline-flex items-center justify-center px-4 py-2.5 dark:bg-gray-700 dark:hover:bg-gray-600 dark:focus:ring-gray-700">
            <FontAwesomeIcon icon={faAndroid} className="w-7 h-7" />
          </div>
          <div class=" sm:w-autofocus:ring-4 focus:outline-none focus:ring-gray-300 text-white inline-flex items-center justify-center px-4 py-2.5 dark:bg-gray-700 dark:hover:bg-gray-600 dark:focus:ring-gray-700">
            <FontAwesomeIcon icon={faWindows} className="w-7 h-7" />
          </div>
        </div>
      </div>

      <div>
        <AppStore />
      </div>
      <div>

        <div class=" p-4 bg-gray-800 md:p-8 dark:border-gray-700 ">
          <dl class="grid max-w-screen-xl grid-cols-3 gap-3 p-4 mx-auto text-white sm:grid-cols-3 xl:grid-cols-3 dark:text-white sm:p-8 justify-center text-center">
            <div class="flex flex-col">
              <dt class="grid grid-cols-2 mb-2 text-3xl font-extrabold">
                <div className="text-yellow-400 text-right"><FontAwesomeIcon icon={faStar} /></div>
                <div className=" text-left"><span class="ml-2 text-sm font-bold text-white">4.95</span></div>
              </dt>
              <dt class="mb-2 text-3xl font-extrabold">73M+</dt>
              <dd class="font-light text-white dark:text-gray-400">Watchs</dd>
            </div>
            <div class="flex flex-col">
              <dt class="grid grid-cols-2 mb-2 text-3xl font-extrabold">
                <div className="text-yellow-400 text-right"><FontAwesomeIcon icon={faStar} /></div>
                <div className=" text-left"><span class="ml-2 text-sm font-bold text-white">4</span></div>
              </dt>
              <dt class="mb-2 text-3xl font-extrabold">68M+</dt>
              <dd class="font-light text-white dark:text-gray-400">Views</dd>
            </div>
            <div class="flex flex-col">
              <dt class="grid grid-cols-2 mb-2 text-3xl font-extrabold">
                <div className="text-yellow-400 text-right"><FontAwesomeIcon icon={faStar} /></div>
                <div className=" text-left"><span class="ml-2 text-sm font-bold text-white">3.50</span></div>
              </dt>
              <dt class="mb-2 text-3xl font-extrabold">52M+</dt>
              <dd class="font-light text-white dark:text-gray-400">Booking</dd>
            </div>
          </dl>
        </div>

      </div>
      <div>
        <Footer />
      </div>
    </>
  );
}
