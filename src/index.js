import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import './index.css';
import Contact from './views/Contact/Contact';
import Cinemas from './views/Cinemas/Cinemas';
import Movie from './views/Home/Movie';
import Layout from './layouts/Layout';
import '../node_modules/font-awesome/css/font-awesome.min.css'; 
// import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <BrowserRouter>
    <Routes>
      <Route path="/" element={<Layout/>}>
        <Route index element={<Movie />}></Route>
        <Route path="cinemas" element={<Cinemas />}></Route>
        <Route path="contact" element={<Contact />}></Route>
      </Route>
    </Routes>
    </BrowserRouter>
  </React.StrictMode>
);
